/**
 * 
 * @param {Map} queryParams A Map containing all of the query parameters
 * @returns {Object} An object containing all the query parameters
 */
async function inspect(queryParams){

    return {}

}

exports = module.exports = inspect


exports.express = ( express ) => {

    express.get( "/inspect", ( req, res ) => {
        let queryParams = new Map()
        inspect(queryParams)
            .then(
                (body) => {
                    res.send( body )
                },
                ()=> {
                    res.set('Content-Type', 'text/plain')
                    res.status(500).send( "Unknown server error" )
                }
            )

    } )

}

exports.fastify = (fastify) => {
    fastify.get(
        '/inspect',
        {
            schema: {
              response: {
                200: {
                  type: 'object',
                  additionalProperties : {
                    type: "string"
                  }
                }
              }
            }
        },
        async function handler (request, reply) {
        let queryParams = new Map()
        let body = await inspect(queryParams)
        reply.send(body)
        return reply
    })
}