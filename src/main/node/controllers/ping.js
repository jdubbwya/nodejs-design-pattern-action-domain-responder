
/**
 * The assumption here is this action always returns a string (text/plain) with
 * the value pong
 * 
 * @returns {string} String pong
 */
async function ping (){
    return "pong"
}

exports = module.exports = ping

exports.express = ( express ) => {

    express.get( "/ping", ( req, res ) => {

        ping()
            .then(
                (body) => {
                    res.set('Content-Type', 'text/plain')
                    res.send( body )
                },
                ()=> {
                    res.set('Content-Type', 'text/plain')
                    res.status(500).send( "Unknown server error" )
                }
            )

    } )

}

exports.fastify = (fastify) => {
    fastify.get('/ping', async function handler (request, reply) {
        let body = await ping()
        reply.header('Content-Type', 'text/plain')
        reply.send( body )
        return reply
    })
}
