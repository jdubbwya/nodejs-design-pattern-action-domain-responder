const express = require('express')
const app = express()
const controllers = require("../controllers")

let controller;

for( mod in controllers ){

    controller = controllers[mod]

    if( controller.express ){
        controller.express( app )
    }

}


app.listen(8080)
