import Fastify from 'fastify'
const fastify = Fastify({
  logger: true
})

import controllers from "../controllers/index.js"

let controller;

for( let mod in controllers ){

    controller = controllers[mod]

    if( controller.fastify ){
        controller.fastify( fastify )
    }

}

(async function(){
  try {
    await fastify.listen({ port: 8080 })
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}) ()