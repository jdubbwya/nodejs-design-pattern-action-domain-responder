#!/usr/bin/env bash

CONTAINER_IMAGE="docker.io/node:18"

function main {

    local script_abs_path=$(dirname $(readlink -f $0))

    PROJECT_ROOT=$(readlink -f "${script_abs_path}/../..")

    local container_runtime="podman"

    if command -v "docker" &>/dev/null
    then
        container_runtime="docker"
    fi

    $container_runtime run --interactive --tty --rm \
        --volume "${PROJECT_ROOT}:/mnt/workspace" \
        --env "WORKSPACE_DIR=/mnt/workspace" \
        --workdir "/mnt/workspace" \
        $CONTAINER_IMAGE \
        /bin/bash

}

main $@