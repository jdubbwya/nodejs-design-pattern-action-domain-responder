# NodeJS - Design Pattern - Action Domain Responder

This repository contains some examples of how to implement the Action Domain Responder (ADR) design pattern. There are two server implementations available fastify and express.

## References

[Action Domain Responder (ADR) pattern](https://pmjones.io/adr/). Paul M. Jones.19 Sept 2023. Action Domain Responder. URL: https://pmjones.io/adr/